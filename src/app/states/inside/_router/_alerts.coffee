angular
  .module 'app'
  .config [
    '$stateProvider'
    ( $stateProvider ) ->

      $stateProvider
        .state 'inside.alerts',
          url : 'alerts'
          templateUrl : 'views/states/inside/_views/_alerts.html'
          controller : 'AlertsCtrl'

  ]

