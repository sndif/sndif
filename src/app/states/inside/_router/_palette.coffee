angular
  .module 'app'
  .config [
    '$stateProvider'
    ( $stateProvider ) ->

      $stateProvider
        .state 'inside.palette',
          url : 'palette'
          templateUrl : 'views/states/inside/_views/_palette.html'

  ]

