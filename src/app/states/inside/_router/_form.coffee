angular
  .module 'app'
  .config [
    '$stateProvider'
    ( $stateProvider ) ->

      $stateProvider
        .state 'inside.form',
          url : 'form'
          templateUrl : 'views/states/inside/_views/_form.html'

  ]

