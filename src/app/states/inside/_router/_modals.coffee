angular
  .module 'app'
  .config [
    '$stateProvider'
    ( $stateProvider ) ->

      $stateProvider
        .state 'inside.modals',
          url : 'modals'
          templateUrl : 'views/states/inside/_views/_modals.html'

        .state 'inside.modals.default',
          url : '/default'
          templateUrl : 'views/states/inside/_views/_modal-default.html'

        .state 'inside.modals.half',
          url : '/half'
          templateUrl : 'views/states/inside/_views/_modal-half.html'

        .state 'inside.modals.large',
          url : '/large'
          templateUrl : 'views/states/inside/_views/_modal-large.html'

        .state 'inside.modals.card',
          url : '/card'
          templateUrl : 'views/states/inside/_views/_modal-card.html'

  ]

