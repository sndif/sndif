angular
  .module 'app'
  .config [
    '$stateProvider'
    ( $stateProvider ) ->

      $stateProvider
        .state 'inside',
          url : '/'
          templateUrl : 'views/states/inside/main.html'

        .state 'inside.logout',
          url : 'logout'

  ]
