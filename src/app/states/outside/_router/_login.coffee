angular
  .module 'app'
  .config [
    '$stateProvider'
    ( $stateProvider ) ->

      $stateProvider
        .state 'outside',
          url : '/login'
          templateUrl : 'views/states/outside/_views/_login.html'

  ]
