angular
  .module 'app'
  .config [
    '$stateProvider'
    ( $stateProvider ) ->

      $stateProvider
        .state 'outside:lock',
          url : '/lock'
          templateUrl : 'views/states/outside/_views/_lock.html'

  ]
